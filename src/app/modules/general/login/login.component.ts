import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent  {
  constructor() { }

   err() {
    console.log('testJsError')
    throw new Error('преднамеренная ошибка')
  }
}
